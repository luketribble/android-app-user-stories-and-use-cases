For my software engineering class, I helped created the Use Cases and User Stories for our application Yummit,
a service that allows people to buy homemade food from others. To practice learning both Waterfall and Agile
methodologies, both Use Cases and User Stories were written.